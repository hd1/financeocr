package us.d8u.finance;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.rowset.WebRowSet;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVWriter;

import com.sun.rowset.WebRowSetImpl;

/**
 * @author hdiwan
 * 
 */
@SuppressWarnings("restriction")
public final class Export {
	private static Logger log = LoggerFactory.getLogger(Export.class);
	private static JsonFactory factory = new JsonFactory();
	
	/**
	 * returns a the user's transactions as a Writer in the desired format
	 * 
	 * @param user
	 *            username
	 * @param format
	 *            output format
	 */
	@SuppressWarnings("resource")
	public static Writer export(String user) throws SQLException {
		Properties configuration = new Properties();

		try {
			InputStream configSrc = Class.forName("us.d8u.finance.Export").getClassLoader().getResourceAsStream("FinanceOCR.properties");
			configuration.load(configSrc);
		} catch (IOException e) {
			log.warn(e.getLocalizedMessage());
		} catch (ClassNotFoundException e) { }
		
		StringWriter ret = new StringWriter();
		Connection conn = null;
		conn = DriverManager
			.getConnection("jdbc:h2:tcp://192.168.1.6/finance;USER=sa");
		PreparedStatement prepped = null;
		prepped = conn
			.prepareStatement(configuration.getProperty("export.transactionsForUser"),
			                  ResultSet.TYPE_SCROLL_INSENSITIVE,
			                  ResultSet.CONCUR_READ_ONLY);
		prepped.setString(1, user);
		// according to
		// http://stackoverflow.com/questions/1107732/how-to-avoid-oom-out-of-memory-error-when-retrieving-all-records-from-huge-tab,
		// this makes memory errors a thing of the ancient past
		prepped.setFetchSize(500);
		ResultSet results = null;
		results = prepped.executeQuery();
		String format = System.getProperty("format");
		if (format.equalsIgnoreCase("csv")) {
			try {
				new CSVWriter(ret).writeAll(results, true);
			} catch (IOException e) {
				// according to http://stackoverflow.com/a/470492/783412, set the logger to log these events
				System.setProperty("java.util.logging.ConsoleHandler.level","FINER");
				log.warn(e.getMessage(),e);
				// according to http://stackoverflow.com/questions/4970513/java-logging-levels-confusion, the default logging level is INFO
				System.setProperty("java.util.logging.ConsoleHandler.level", "INFO");		
			}
		} else if (format.equalsIgnoreCase("json")) {
			try {
				JsonGenerator generator = factory.createJsonGenerator(ret);
				generator.useDefaultPrettyPrinter();
				generator.writeStartArray();
				do {
					results.next();
					generator.writeStartObject();
					generator
						.writeStringField("location", WordUtils.capitalize(results.getString(1)));
					generator.writeNumberField("Amount", results.getDouble(2));
					generator.writeStringField("Transaction time",
					                           results.getString(3));
					generator.writeEndObject();
				} while (results.isLast() == false);
				generator.writeEndArray();
				generator.close();
			} catch (IOException e) {
				log.warn(e.getMessage(), e);
			}
		} else if (format.equalsIgnoreCase("xml")) {
			WebRowSet wrs = new WebRowSetImpl();
			wrs.populate(results);
			wrs.writeXml(ret);
		} else if (format.equalsIgnoreCase("xls")) {
			ret = (StringWriter) writeExcel(user, results);
		}
		conn.close();
		return ret;
	}

	private static Writer writeExcel(String user, ResultSet results) {
		// http://stackoverflow.com/questions/7274076/writing-a-large-resultset-to-an-excel-file-using-poi
		BufferedOutputStream bos = null;
		try {
			bos = new BufferedOutputStream(new FileOutputStream(
			                                                    "/home/hdiwan/public_html/Finance/" + user + "/txns.xls"));
		} catch (FileNotFoundException e4) {
			e4.printStackTrace(System.err);
			System.exit(-1);
		}
		int numColumns = 0;
		try {
			numColumns = results.getMetaData().getColumnCount();
		} catch (SQLException e3) {
			e3.printStackTrace(System.err);
			System.exit(-1);
		}

		Workbook wb = new HSSFWorkbook();
		Sheet theSheet = wb.createSheet();
		Row heading = theSheet.createRow(0);

		ResultSetMetaData rsmd = null;
		try {
			rsmd = results.getMetaData();
		} catch (SQLException e2) {
			e2.printStackTrace(System.err);
			//			System.exit(-1);
		}

		for (int x = 0; x < numColumns; x++) {
			Cell cell = heading.createCell(x);
			try {
				cell.setCellValue(rsmd.getColumnLabel(x + 1));
			} catch (SQLException e) {
				e.printStackTrace(System.err);
				//				System.exit(-1);
			}
		}

		int rowNumber = 1;
		int sheetNumber = 0;

		try {
			do {
				if (rowNumber % 65001 == 0) {
					log.info("Sheet " + sheetNumber
					         + "full; moving onto to sheet " + (sheetNumber + 1));
					sheetNumber++;
					rowNumber = 1;
				}
				results.next();
				Row row = wb.getSheetAt(sheetNumber).createRow(rowNumber++);
				for (int y = 0; y < numColumns; y++) {
					try {
						Cell cell = row.createCell(y);
						cell.setCellValue(
						                  WordUtils.capitalize(results.getString(y + 1)));
					} catch (SQLException e1) {
						e1.printStackTrace(System.err);
					}
				}
			} while (results.isLast() == false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (int column = 0; column < numColumns; column++) {
			wb.getSheetAt(sheetNumber).autoSizeColumn(column);
		}
		try {
			wb.write(bos);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
		try {
			bos.close();
		} catch (IOException e) {
			e.printStackTrace(System.err);
			//			System.exit(-1);
		}
		StringWriter ret = new StringWriter();
		ret.append("/home/hdiwan/public_html/Finance/" + user + "/txns.xls");
		return ret;
	}
}
