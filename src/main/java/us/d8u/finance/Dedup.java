package us.d8u.finance;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Dedup {
	Connection conn;
	private static final Logger log = LoggerFactory.getLogger(Dedup.class);
	final Properties configuration = new Properties();
	public Dedup() throws SQLException {
		InputStream configSrc = this.getClass().getClassLoader().getResourceAsStream("FinanceOCR.properties");
		try {
			configuration.load(configSrc);
		} catch (IOException e) {
			log.warn(e.getMessage(), e);
		}
		try {
			Class.forName("org.h2.Driver").newInstance();
		} catch (ClassNotFoundException e) {
			log.warn(e.getMessage(), e);
		} catch (InstantiationException e) {
			log.warn(e.getMessage(), e);
		} catch (java.lang.IllegalAccessException e) {
			log.warn(e.getMessage(), e);
		}
		try {
			conn = DriverManager.getConnection(configuration.getProperty("jdbc.url"));
			conn.setAutoCommit(false);
			ResultSet total = conn.createStatement().executeQuery(configuration.getProperty("dedup.totalcount"));
			total.next();
			int previousRowCount = total.getRow();
			conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE).execute(configuration.getProperty("dedup.statement"));
			total = conn.createStatement().executeQuery(configuration.getProperty("dedup.totalcount"));
			total.next();
			int deleted = previousRowCount - total.getRow();
			long start = System.currentTimeMillis();
			conn.commit();
			log.info("Took "+(System.currentTimeMillis()-start)+" miliseconds to delete "+deleted+" duplicate transactions");
		} catch (SQLException e) {
			log.warn(e.getMessage(), e);
		} finally {
			conn.close();
		}
	}
	public static void main (String[] args) {
		try {
			new Dedup();
		} catch (SQLException e) {
			log.warn(e.getMessage(), e);
		}
	} 
}
