package us.d8u.finance;
/** 
 * Adds a user with 1 or more valid email addresses
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class AddUser {
	private static Logger log = LoggerFactory.getLogger(AddUser.class);
	public static void main(String[] args) {
		if (args.length < 2) {
			throw new ArrayIndexOutOfBoundsException("Need Username and email address");
		}
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:h2:tcp://localhost/finance;USER=sa");
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage());
			System.exit(1);
		}
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage());
		}
		PreparedStatement prepped = null;
		try {
			prepped = connection.prepareStatement("insert into app_users (name) values (?)", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			prepped.setString(1, args[0]);
			prepped.executeUpdate();
		} catch (SQLException e) {
			log.info("User "+args[0]+" already exists. Just adding email address(es).");
		} finally { // username already exists, just a new email address
			try {
				prepped = connection.prepareStatement("insert into user_emails (user_id, email) values (?, ?)");
				PreparedStatement findUserId = connection.prepareStatement("select id from app_users where name = ?", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        findUserId.setString(1, args[0]);
        ResultSet userId = findUserId.executeQuery();
				userId.next();
				prepped.setInt(1, userId.getInt(1));
				for (int i = 1; i != args.length;i++) {
					prepped.setString(2,args[i]);
					prepped.executeUpdate();
				}
				connection.commit();
				connection.close();
			} catch (SQLException e) {
				log.warn(e.getLocalizedMessage());
				System.exit(-1);
			}
		}
	}
}
