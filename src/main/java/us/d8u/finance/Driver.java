package us.d8u.finance;

import java.io.StringWriter;
import java.sql.SQLException;

import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Driver {
	private static Logger log = LoggerFactory.getLogger(Driver.class);

	public static void handleException(Throwable e) throws RuntimeException {
		log.warn(e.getMessage());
	}

	public static void main(String[] args) {
		// create Options object
		Options options = new Options();

		// add options
		options.addOption("i", false, "import from email");
		options.addOption("t", false, "import from twitter");
		options.addOption("e", false,
				"export to that specified by the format property");
		options.addOption("s", false, "Setup database");
		options.addOption("d", false, "delete duplicate records");

		CommandLineParser parser = new PosixParser();
		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			Driver.handleException(e);
		}
		if (cmd.hasOption("i")) {
			ImportFromMail.main(null);
		}
		if (cmd.hasOption("t")) {
			new ImportFromTwitter().run();
		}
		if (cmd.hasOption("s")) {
			try {
				Install.main(null);
			} catch (InstantiationException e) {
				Driver.handleException(e);
			} catch (IllegalAccessException e) {
				Driver.handleException(e);
			} catch (ClassNotFoundException e) {
				Driver.handleException(e);
			}
		}
		if (cmd.hasOption("e")) {
			StringWriter w = null;
			if (System.getProperty("username") == null) {
				throw new RuntimeException(
						"username property must be defined for the export!");
			}
			if (System.getProperty("format") == null) {
				System.setProperty("format", "csv");
			}
			try {
				w = (StringWriter) Export
						.export(System.getProperty("username"));
			} catch (SQLException e) {
				Driver.handleException(e);
			}
			System.out.println(w.toString());
		} else if (cmd.hasOption("d")) {
			Dedup.main(null);
		} else { // options is not valid
			new HelpFormatter().printHelp("Driver", options);
			System.exit(-1);
		}
	}
}