#!/usr/bin/perl
use warnings;
use strict;
use diagnostics;
use CGI::Pretty;
use POSIX qw/strftime/;
my $q = new CGI::Pretty;
my $fh;
my $base = "http://d8u.us/~hdiwan/visualisations/$ARGV[0]";
chdir "/home/hdiwan/public_html/Finance/$ARGV[0]";
open $fh, '>', "/home/hdiwan/public_html/Finance/$ARGV[0]/index.html" or die $!;
my $now = strftime('%c',localtime);
print $fh $q->start_html("Visualisations for $ARGV[0]"), $q->div({-align => 'right', -valign=>'top'}, $q->font({-size=>'-2'}, "As of $now")), $q->br;
foreach my $png (glob('*.png')) { print $fh "<a href=\"$png\"><img src=\"$png\" height=\"33%\" width=\"33%\" alt=\"$png\"/></a>"; }
print $fh $q->br(),$q->div({-align=>'center'},$q->a({-href=> "$base/txns.csv.bz2"}, 'CSV'). '&nbsp;-&nbsp;'. $q->a({-href=>"$base/txns.xml.bz2"}, 'XML'). '&nbsp;-&nbsp;'. $q->a({-href=>"$base/txns.json.bz2"}, 'JSON'). '&nbsp;-&nbsp;', $q->a({-href=>"$base/txns.xls"}, 'XLS')), $q->br();
print $fh $q->end_html;
close $fh;
